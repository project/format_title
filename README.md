# Formatted titles

This module allows to change the type of the title field in an entity's edit
form from simple text to formatted text. When the entity is loaded, its title
is then replaced with its formatted version.

In a view, Formatted Title tries to replace all instances of an entity's title,
even when the entity is being brought in through a relationship.

After enabling the module, go to `admin/config/content/format-title`, where you
can enable formatted titles on a per-entity, per-bundle basis. You can also
choose to restrict the HTML output of the module even further.