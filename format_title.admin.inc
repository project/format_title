<?php
/**
 * @file
 * Administrative page callbacks for format_title module.
 */

/**
 * Form constructor for settings page.
 */
function format_title_settings() {
  $entities = format_title_get_entity_properties();

  if (!empty($entities)) {
    $formats = format_title_filter_formats();

    $table = array(
      'header' => array(
        'entity' => t('Entity'),
        'bundles' => t('Enabled bundles'),
        'format' => t('Format'),
      )
    );

    foreach ($entities as $entity => $properties) {
      // Build list of enabled bundles
      $enabled_bundles = array();
      foreach ($properties['bundles'] as $bundle => $bundle_properties) {
        $setting = variable_get("format_title:$entity:$bundle", 0);
        if (!empty($setting)) {
          $enabled_bundles[$bundle] = $bundle_properties['label'];
        }
      }

      $format = format_title_get_entity_format($entity);
      $enabled_bundles = empty($enabled_bundles) ? t('No bundles enabled') : implode(', ', $enabled_bundles);

      $table['rows'][] = array(
        'entity' => l(t($properties['name']), 'admin/config/content/format-title/' . $entity),
        'bundles' => $enabled_bundles,
        'format' => empty($format) ? t('- None -') : $formats[$format],
      );
    }

    $form['format_title_entities'] = array(
      '#markup' => theme('table', $table)
    );
  }

  $default_format = variable_get('format_title_default_format');
  if (empty($default_format)) {
    drupal_set_message(l(t('You must select a default text format for your formatted titles.'), current_path(), array('fragment' => 'edit-format-title-default-format')), 'warning');
  }

  $form['format_title_default_format'] = array(
    '#type' => 'select',
    '#title' => t('Default text format'),
    '#description' => t('Select a format that all appropriate roles can access.'),
    '#options' => $formats,
    '#default_value' => $default_format,
  );

  return system_settings_form($form);
}

/**
 * Form constructor for entity-specific settings page.
 */
function format_title_entity_settings($form, &$form_state, $entity_type) {
  $entity = format_title_get_entity_properties($entity_type);
  $form_state['build_info']['entity'] = $entity;
  extract($entity);

  drupal_set_title(t('!entity Formatted Title settings', array('!entity' => $name)));

  if (!empty($bundles)) {
    $settings = array();
    foreach ($bundles as $bundle_name => $bundle) {
      $settings[$bundle_name] = variable_get("format_title:$entity_type:$bundle_name", 0);
    }

    $form['warning'] = array(
      '#markup' => t('Please note that these settings affect only entity title fields set at the root of the form. Placing a title field within a field group will prevent Formatted Titles from working properly.'),
    );

    $form['format_title_settings'] = array(
      '#type' => 'container',
      '#tree' => TRUE,
    );

    $form['format_title_settings'][$entity_type] = array(
      '#type' => 'checkboxes',
      '#title' => t('Bundles'),
      '#description' => t('Choose which bundles will have a formatted title field.'),
      '#default_value' => $settings,
    );

    foreach ($bundles as $bundle_name => $bundle) {
      $form['format_title_settings'][$entity_type]['#options'][$bundle_name] = t($bundle['label']);
    }

    $formats = format_title_filter_formats();

    $form['format_title_format'] = array(
      '#type' => 'select',
      '#title' => t('Default text format'),
      '#description' => t('Select a format that all appropriate roles can access.'),
      '#options' => $formats,
      '#default_value' => format_title_get_entity_format($entity_type),
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save settings'),
    );
  }
  else {
    $form['empty'] = array(
      '#markup' => t('There are no available bundles for the !entity entity.', array('!entity' => $name)),
    );
  }

  return $form;
}

/**
 * Form submission handler for format_title_entity_settings().
 */
function format_title_entity_settings_submit($form, &$form_state) {
  $entity_type = $form_state['build_info']['args'][0];
  $settings = $form_state['values']['format_title_settings'][$entity_type];

  foreach ($settings as $bundle => $value) {
    variable_set("format_title:$entity_type:$bundle", $value);
  }

  if (!empty($form_state['values']['format_title_format'])) {
    variable_set("format_title_format:$entity_type", $form_state['values']['format_title_format']);
  }

  drupal_set_message(t('Formatted Title settings for %entity entities have been saved.', array('%entity' => t($form_state['build_info']['entity']['name']))));
  $form_state['redirect'] = 'admin/config/content/format-title';
}

/**
 * Returns an array of active text formats.
 */
function format_title_filter_formats() {
  $formats = filter_formats();
  $formats = array_filter($formats, function($format) {
    return $format->status;
  });
  array_walk($formats, function(&$format) {
    $format = $format->name;
  });

  return $formats;
}
